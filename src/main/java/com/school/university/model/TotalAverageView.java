package com.school.university.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "total_averages", schema = "university")
@Builder
public class TotalAverageView {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "student_id")
    @OneToOne
    @JsonIgnore
    private Student studentId;

    @Column(name = "value")
    private Double value;
}
