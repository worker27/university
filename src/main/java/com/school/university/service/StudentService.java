package com.school.university.service;

import com.school.university.model.*;
import com.school.university.model.DTO.StudentRegisterDTO;

import java.util.List;
import java.util.Map;

public interface StudentService {

    public List<PhoneType> getAllPhoneType();

    public List<Group> getAllGroups();

    public List<Discipline> getAllDiscipline();

    public List<Average> calculateAveragePerStudent(Student student);

    public TotalAverageView calculateTotalAveragePerStudent(Student student);

    public Map<String, List<Student>> calculateBursaByStudents(List<Student> students);

    public Double calculateAveragePerGroup(Group group);

}
