package com.school.university.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Table(name = "teachers", schema = "university")
@Entity
public class Teacher extends Person {

    @Column(nullable = false, scale = 2)
    private Double salary;


    public Teacher(Long id) {
        super(id);
    }

    public Teacher() {}
}
