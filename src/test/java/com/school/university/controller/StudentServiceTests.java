package com.school.university.controller;

import com.school.university.model.*;
import com.school.university.repository.GroupRepository;
import com.school.university.repository.MarkRepository;
import com.school.university.repository.StudentRepository;
import com.school.university.service.StudentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource("/application.properties")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Service.class, Repository.class}))
public class StudentServiceTests {

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private MarkRepository markRepository;

    @Autowired
    private GroupRepository groupRepository;

    private List<Average> getAverages() {
        List<Average> averages = new ArrayList<>();
        averages.add(Average.builder().student(new Student(1L)).discipline(new Discipline(1L)).value(7.0).build());
        averages.add(Average.builder().student(new Student(1L)).discipline(new Discipline(2L)).value(6.6).build());
        averages.add(Average.builder().student(new Student(1L)).discipline(new Discipline(4L)).value(7.6).build());
        averages.add(Average.builder().student(new Student(1L)).discipline(new Discipline(8L)).value(6.8).build());
        averages.add(Average.builder().student(new Student(1L)).discipline(new Discipline(9L)).value(8.0).build());
        return averages;
    }

    @Test
    public void getAllPhoneTypeTest() {
        List<PhoneType> phoneTypes = this.studentService.getAllPhoneType();
        assertNotNull(phoneTypes, "Can not be null!");
        assertEquals(2, phoneTypes.size(), "Wrong size of objects!");
    }

    @Test
    public void getGroupsTest() {
        List<Group> groups = this.studentService.getAllGroups();
        assertNotNull(groups, "Can not be null!");
        assertEquals(5, groups.size(), "Wrong size of objects!");
    }

    @Test
    public void getAllDisciplineTest() {
        List<Discipline> disciplines = this.studentService.getAllDiscipline();
        assertNotNull(disciplines, "Can not be null!");
        assertEquals(12, disciplines.size(), "Wrong size of objects!");
    }

    @Test
    public void calculateStudentAverageTest() throws Exception {
        Student student = studentRepository.getOne(1L);
        Set<Discipline> disciplines = student.getDisciplines();
        long markCount;

        for (Discipline discipline : disciplines) {
            markCount = student.getMarks().stream().filter(mark -> mark.getDiscipline().getId().equals(discipline.getId())).count();
            assertEquals( 5, markCount, "Student can not have more less than 5 marks per discipline!!!");
        }

        List<Average> averages = this.studentService.calculateAveragePerStudent(new Student(1L));

        assertNotNull(averages, "This list can not be null");
        assertEquals(5, averages.size(), "Something is wrong");

        List<Average> expectedAverages = getAverages();
        Iterator iterator = averages.iterator();

        for (Average average : expectedAverages) {
            Average actualAverage = ((Average) iterator.next());

            assertEquals(average.getStudent().getId(), actualAverage.getStudent().getId(), "Wrong student Id");
            assertEquals(average.getValue(), actualAverage.getValue(), "Actual values doesn't equals with expected values");
            assertEquals(average.getDiscipline().getId(), actualAverage.getDiscipline().getId(), "Actual values doesn't equals with expected values");
        }
    }

    @ParameterizedTest
    @CsvSource(value = {"7.733333333333334:1"}, delimiter = ':')
    public void calculateTotalAveragePerStudentTest(Double expectedAverage, Long studentId) throws Exception {
        TotalAverageView totalAverageView = this.studentService.calculateTotalAveragePerStudent(new Student(studentId));

        assertNotNull(totalAverageView, "Must be not null!");
        assertEquals(expectedAverage, totalAverageView.getValue(), "Total average is not calculated properly!");
    }

    @Test
    public void calculateBursaByStudentsTest() {
        List<Student> studentList = this.studentRepository.findAll();

        assertNotNull(studentList, "This list can not be null!");

        for (Student student : studentList) {
            student.setAverages(this.studentService.calculateAveragePerStudent(student));
            student.setTotalAverage(this.studentService.calculateTotalAveragePerStudent(student));

            assertNotNull(student.getAverages(), "Averages can not be null!");
            assertNotNull(student.getTotalAverage(), "Total average can not be null!");
        }

        Map<String, List<Student>> studentMap = this.studentService.calculateBursaByStudents(studentList);

        assertNotNull(studentMap.get("studentsWithBursa"), "Can not be null!");
        assertNotNull(studentMap.get("studentsExcomunicado"), "Can not be null!");

        assertEquals(2, studentMap.get("studentsWithBursa").size(), "Wrong size of list");
        assertEquals(1, studentMap.get("studentsExcomunicado").size(), "Wrong size of list");

        for (Student student : studentMap.get("studentsWithBursa")) {
            assertTrue(student.getTotalAverage().getValue() >= 8.0);
        }

        for (Student student : studentMap.get("studentsExcomunicado")) {
            assertTrue(student.getTotalAverage().getValue() < 5.0);
        }
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 2, 3, 4, 5})
    public void calculateAveragePerGroupTest(Long groupId) {
        List<Student> studentList = this.groupRepository.getOne(groupId).getStudents();

        assertNotNull(studentList, "This list can not be null!");

        for (Student student : studentList) {
            student.setAverages(this.studentService.calculateAveragePerStudent(student));
            student.setTotalAverage(this.studentService.calculateTotalAveragePerStudent(student));

            assertNotNull(student.getAverages(), "Averages can not be null!");
            assertNotNull(student.getTotalAverage(), "Total average can not be null!");
        }

        Double groupAverage = this.studentService.calculateAveragePerGroup(new Group(groupId));
        assertNotNull(groupAverage, "Can not be null!");
        assertTrue(groupAverage > 0, "Must be greater than zero!");
        assertTrue(groupAverage <= 10, "Must be equal or less than ten!");
    }
}
