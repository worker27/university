package com.school.university.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "groups", schema = "university")
@AllArgsConstructor
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Min(value = 1, message = "Group cannot be null")
    private Long id;

    @Column(nullable = false, unique = true ,length = 20)
    @Size(max = 20, message = "Group name should not be longer than 20")
    private String name;

    @OneToMany(mappedBy = "group", targetEntity = Student.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Student> students;

    public Group() {}

    public Group(Long id) {
        this.id = id;
    }
}
