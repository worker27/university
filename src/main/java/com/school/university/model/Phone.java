package com.school.university.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Table(name = "phones", schema = "university")
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, length = 25)
    @Size(max = 25, message = "Your phone number should not be longer than 25 and shorter than 5 digits")
    private String value;

    @ManyToOne
    @Valid
    @JoinColumn(name = "type_id", nullable = false)
    private PhoneType phoneType;
}
