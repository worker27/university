package com.school.university.model.enums;

public enum Status {
    Active,
    Suspended,
    None
}
