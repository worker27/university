package com.school.university.service.impl;

import com.school.university.model.*;
import com.school.university.model.DTO.StudentRegisterDTO;
import com.school.university.repository.*;
import com.school.university.service.StudentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {

    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private StudentRepository studentDao;

    @Autowired
    private AddressRepository addressDao;

    @Autowired
    private GroupRepository groupDao;

    @Autowired
    private PhoneRepository phoneDao;

    @Autowired
    private LibraryAbonamentRepository libraryAbonamentDao;

    @Autowired
    private PhoneTypeRepository phoneTypeDao;

    @Autowired
    private AverageRepository averageDao;

    @Autowired
    private DisciplineRepository disciplineDao;

    @Autowired
    private MarkRepository markDao;

    @Autowired
    private TeacherRepository teacherDao;

    @Autowired
    private TotalAverageViewRepository totalAverageViewRepository;

    @Override
    public List<PhoneType> getAllPhoneType() {
        return phoneTypeDao.findAll();
    }

    @Override
    public List<Group> getAllGroups() {
        return groupDao.findAll();
    }

    @Override
    public List<Discipline> getAllDiscipline() {
        return disciplineDao.findAll();
    }

    @Override
    public List<Average> calculateAveragePerStudent(Student student) {

        Student student1 = studentDao.getOne(student.getId());

        Set<Discipline> studentDisciplines = student1.getDisciplines();
        Map<Long, double[]> studentMarks = new HashMap<>();
        List<Average> studentAverages = new ArrayList<>();

        studentDisciplines.iterator().forEachRemaining(discipline -> studentMarks.put(discipline.getId(), student1.getMarks().stream().filter(mark -> mark.getDiscipline().equals(discipline)).mapToDouble(Mark::getValue).toArray()));
        studentMarks.forEach((k, v) -> studentAverages.add(Average.builder().discipline(new Discipline(k)).student(new Student(student.getId())).value(Arrays.stream(v).average().getAsDouble()).build()));

        return averageDao.saveAll(studentAverages);
    }

    @Override
    public TotalAverageView calculateTotalAveragePerStudent(Student student) {

        List<Average> averages = averageDao.loadAllAverageByStudent(student.getId());

        double[] averageValues = averages.stream().mapToDouble(Average::getValue).toArray();

        Double totalAverage = Arrays.stream(averageValues).average().getAsDouble();

        return totalAverageViewRepository.save(TotalAverageView.builder().studentId(student).value(totalAverage).build());
    }

    @Override
    public Map<String, List<Student>> calculateBursaByStudents(List<Student> students) {

        Map<String, List<Student>> studentMap = new HashMap<>();

        List<Student> studentList = studentDao.findAllById(students.stream().map(Student::getId).collect(Collectors.toList()));

        List<Student> studentsWithBursa = new ArrayList<>();
        List<Student> studentsExcomunicado = new ArrayList<>();

        for (Student student : studentList) {

            if (student.getTotalAverage().getValue() >= 8.0) {
                studentsWithBursa.add(student);
            } else {
                if (student.getTotalAverage().getValue() < 5.0) {
                    studentsExcomunicado.add(student);
                }
            }
        }

        studentMap.put("studentsWithBursa", studentsWithBursa);
        studentMap.put("studentsExcomunicado", studentsExcomunicado);

        return studentMap;
    }

    @Override
    public Double calculateAveragePerGroup(Group group) {

        Group groupDaoOne = groupDao.getOne(group.getId());

        double[] averages = groupDaoOne.getStudents().stream().mapToDouble(value -> value.getTotalAverage().getValue()).toArray();

        return Arrays.stream(averages).average().getAsDouble();
    }

}
