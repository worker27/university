package com.school.university.repository;

import com.school.university.model.PhoneType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneTypeRepository extends JpaRepository<PhoneType, Long> {
}
