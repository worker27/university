package com.school.university.controller;

import com.school.university.model.*;
import com.school.university.model.DTO.StudentRegisterDTO;
import com.school.university.service.StudentService;
import com.school.university.service.impl.StudentServiceImpl;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StudentControllerTests {

    @LocalServerPort
    private int port;

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void findAllBySearchTest() throws Exception {

        ResponseEntity<String> response = testRestTemplate.getForEntity(
                new URL("http://localhost:" + port + "/students").toString(), String.class);
        assertNotNull(response.getBody(), "Probably is something wrong with your database!");
    }

    @Test
    public void phoneTypesTest() throws Exception {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
                new URL("http://localhost:" + port + "/phoneTypes").toString(), String.class);
        assertNotNull(response.getBody(), "Probably is something wrong with your database!");
    }

    @Test
    public void createStudent() throws Exception {
        ResponseEntity<String> response = testRestTemplate.postForEntity(
                new URL("http://localhost:" + port + "/addStudent").toString(), new Student(), String.class);
        assertNotNull(response.getBody(), "Probably is something wrong with your database!");
    }


}
