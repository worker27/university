package com.school.university.repository;

import com.school.university.model.LibraryAbonament;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LibraryAbonamentRepository extends JpaRepository<LibraryAbonament, Long> {
}
