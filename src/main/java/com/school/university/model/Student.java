package com.school.university.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Table(name = "students", schema = "university")
@Entity
@AllArgsConstructor
public class Student extends Person {

    @ManyToMany(targetEntity = Discipline.class,cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "disciplines_to_students", schema = "university",
            joinColumns = {@JoinColumn(name = "student_id")},
            inverseJoinColumns = {@JoinColumn(name = "discipline_id")})
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<Discipline> disciplines = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    @OneToMany(mappedBy = "student", targetEntity = Average.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Average> averages;

    @OneToOne(mappedBy = "studentId", targetEntity = TotalAverageView.class)
    private TotalAverageView totalAverage;

    @OneToMany(mappedBy = "student", targetEntity = Mark.class, fetch = FetchType.EAGER)
    private List<Mark> marks;

    public Student(Long id) {
        super(id);
    }

    public Student(Long id, String firstName, String lastName) {
        super(id, firstName, lastName);
    }

    public Student(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public Student() {
    }
}
