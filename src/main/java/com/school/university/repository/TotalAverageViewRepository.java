package com.school.university.repository;

import com.school.university.model.TotalAverageView;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TotalAverageViewRepository extends JpaRepository<TotalAverageView, Long> {
}
