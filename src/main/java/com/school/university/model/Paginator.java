package com.school.university.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Paginator {
    private Long studentsCount;
    private List<Student> students;
}
