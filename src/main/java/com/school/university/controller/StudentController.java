package com.school.university.controller;

import com.school.university.model.*;
import com.school.university.model.DTO.StudentRegisterDTO;
import com.school.university.repository.MarkRepository;
import com.school.university.repository.StudentRepository;
import com.school.university.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class StudentController {

    @Resource
    private StudentService studentService;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private MarkRepository markRepository;

    @GetMapping(value = "/students")
    public String findAllBySearch(Search search) {

        Integer currentPage = search.getPage();

        Integer firstResult = (currentPage == null) ? 0 : (currentPage - 1) * search.getSize();

        Paginator paginator = new Paginator();
        paginator.setStudentsCount(studentRepository.count());
        paginator.setStudents(studentRepository.findAll());
        return paginator.toString();
    }

    @GetMapping(value = "/phoneTypes")
    public List<PhoneType> phoneTypes() {
        return studentService.getAllPhoneType();
    }

    @GetMapping(value = "/groups")
    public List<Group> getAllGroups() {
        return studentService.getAllGroups();
    }

    @GetMapping(value = "/disciplines")
    public List<Discipline> getAllDisciplines() {
        return studentService.getAllDiscipline();
    }

    @GetMapping(value = "/student/{id}/calculate")
    public List<Average> getStudentAverages(@PathVariable Long id) {
        return studentService.calculateAveragePerStudent(studentRepository.getOne(id));
    }
}
