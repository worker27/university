package com.school.university.repository;

import com.school.university.model.Average;
import java.util.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AverageRepository extends JpaRepository<Average, Long> {

    @Query("SELECT a FROM Average a WHERE a.student.id = ?1")
    public List<Average> loadAllAverageByStudent(Long studentId);
}
