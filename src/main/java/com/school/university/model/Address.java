package com.school.university.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Table(name = "addresses", schema = "university")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    @Size(max = 30, message = "Country should not be longer than 30")
    private String country;

    @Column(nullable = false, length = 30)
    @Size(max = 30, message = "City should not be longer than 30")
    private String city;

    @Column(nullable = false, length = 30)
    @Size(max = 30, message = "Address should not be longer than 20")
    private String address;

    @Formula("concat(country,' ',city,' ',address)")
    private String concatAddress;

}