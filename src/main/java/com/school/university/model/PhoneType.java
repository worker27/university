package com.school.university.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Table(name = "phone_types", schema = "university")
@AllArgsConstructor
public class PhoneType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    @Size(min = 10, message = "Phone type name should not be longer than 10")
    private String name;

    public PhoneType() {}
}
