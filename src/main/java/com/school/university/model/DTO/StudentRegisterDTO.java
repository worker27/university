package com.school.university.model.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.school.university.model.Address;
import com.school.university.model.Group;
import com.school.university.model.LibraryAbonament;
import com.school.university.model.Phone;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Set;

@Data
@Getter
@Setter
@Builder
public class StudentRegisterDTO {

    private Long id;

    @Size(max = 20, message = "First name should not be longer than 20")
    private String firstName;

    @Size(max = 20, message = "Last name should not be longer than 20")
    private String lastName;

    private byte[] picture;

    @NotNull(message = "Date of birth cannot be null")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateOfBirth;

    @NotNull(message = "Gender cannot be null")
    private Character gender;

    @Valid
    private Address address;

    @Valid
    private Set<Phone> phones;

    @Valid
    private Group group;

    @Valid
    private List<Phone> dublicatePhone = new ArrayList<Phone>();

    private LibraryAbonament libraryAbonament;

    @Transient
    public String getBase64FromByteArray() {
        return Base64.getEncoder().encodeToString(picture);
    }
}
